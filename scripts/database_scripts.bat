@ECHO OFF

if [%1]==[] goto no_argument

set sql_directory=%~dp0\..\sql_%1

REM initializing log
echo %date% %time% [INFO] Starting %1 SQL script.

REM confirm working SQL directory
echo %date% %time% [INFO] %sql_directory%

REM drop table
echo %date% %time% [INFO] Executing %1 SQL...
snowsql --config C:/Users/barry/.snowsql/config -c trial_bformus -w COMPUTE_WH -d JENKINS_POC -s PUBLIC -f "%sql_directory%\%1.sql" -o exit_on_error=true

REM get snowsql exit code
set exit_status=%errorlevel%

REM check that snowsql script successfully completed
if %exit_status% NEQ 0 goto error

goto end

:no_argument
	echo %date% %time% [FAILURE] Jenkins stage argument not supplied.
	exit /B 1

:error
	echo %date% %time% [FAILURE] There was a failure in the SQL script execution.
	exit /B 1
	
:end
	echo %date% %time% [SUCCESS] %1 SQL executed successfully.
	exit /B 0