@ECHO OFF

REM set directory date variables YYYYMMDD
set directory_append_date=%date:~10,4%%date:~4,2%%date:~7,2%

REM set file datetime variable YYYYMMDD_HHMMSS
set file_append_datetime=%date:~-4%%date:~4,2%%date:~7,2%_%time:~0,2%%time:~3,2%%time:~6,2%

REM set directory variables
set working_directory=%~dp0\..\
set deploy_directory=%working_directory%\sql_deploy\
set deploy_archive_directory=%deploy_directory%\archive\%directory_append_date%
set revert_directory=%working_directory%\sql_revert\
set revert_archive_directory=%revert_directory%\archive\%directory_append_date%
set verify_directory=%working_directory%\sql_verify\
set verify_archive_directory=%verify_directory%\archive\%directory_append_date%
set predeploy_directory=%working_directory%\sql_predeploy\
set predeploy_archive_directory=%predeploy_directory%\archive\%directory_append_date%
set cleanup_directory=%working_directory%\sql_cleanup\
set cleanup_archive_directory=%cleanup_directory%\archive\%directory_append_date%

REM create directories, ignore errors.
md "%deploy_archive_directory%" 2>nul
md "%revert_archive_directory%" 2>nul
md "%verify_archive_directory%" 2>nul
md "%predeploy_archive_directory%" 2>nul
md "%cleanup_archive_directory%" 2>nul

REM append datetime and move current SQL files
for %%a in ("%deploy_directory%\*.sql") do ren "%%~a" "%%~Na_%file_append_datetime%%%~Xa"
move %deploy_directory%\*.sql %deploy_archive_directory%

for %%a in ("%revert_directory%\*.sql") do ren "%%~a" "%%~Na_%file_append_datetime%%%~Xa"
move %revert_directory%\*.sql %revert_archive_directory%

for %%a in ("%verify_directory%\*.sql") do ren "%%~a" "%%~Na_%file_append_datetime%%%~Xa"
move %verify_directory%\*.sql %verify_archive_directory%

for %%a in ("%predeploy_directory%\*.sql") do ren "%%~a" "%%~Na_%file_append_datetime%%%~Xa"
move %predeploy_directory%\*.sql %predeploy_archive_directory%

for %%a in ("%cleanup_directory%\*.sql") do ren "%%~a" "%%~Na_%file_append_datetime%%%~Xa"
move %cleanup_directory%\*.sql %cleanup_archive_directory%

REM change to main project folder
cd %working_directory%

REM commit and push changes to git
call git add .
call git commit -m "Successfully deployed %date% %time% changes."
call git push origin master